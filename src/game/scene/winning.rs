use super::Scene;
use crate::prelude::*;

pub struct WinningScene {
    moves: i32,
    has_ended: bool,
}

impl WinningScene {
    pub fn new(moves: i32) -> Self {
        Self {
            moves,
            has_ended: false,
        }
    }
}

impl Scene for WinningScene {
    fn handle_events(&mut self, events: &mut VecDeque<GameEvent>) {
        while let Some(event) = events.pop_front() {
            match event {
                GameEvent::KeyPress(key, _) => {
                    if key == KeyCode::Return || key == KeyCode::NumpadEnter {
                        self.has_ended = true;
                    }
                }
                _ => (),
            }
        }
    }

    fn update(&mut self, events: &mut VecDeque<GameEvent>) {
        if self.has_ended {
            events.push_back(GameEvent::PopScene);
        }
    }

    fn draw(&self, ctx: &mut Context) -> GameResult {
        let mut canvas = Canvas::from_frame(ctx, Color::BLACK);
        let blocks = vec![
            TextBlock::new(
                TextFragment::new("Nice work, you solved this level!")
                    // .font("Videotype")
                    .color(Color::GREEN)
                    .scale(20.0),
                (16.0, 0.0, 0.0, 0.0),
                TextAlign::Middle,
            ),
            TextBlock::new(
                TextFragment::new(format!("Moves: {}", self.moves))
                    .color(Color::WHITE)
                    .scale(40.0),
                (20.0, 0.0, 0.0, 0.0),
                TextAlign::Middle,
            ),
            TextBlock::new(
                TextFragment::new("Press ENTER to play the next level")
                    // .font("Videotype")
                    .color(Color::WHITE)
                    .scale(16.0),
                (16.0, 0.0, 0.0, 0.0),
                TextAlign::Middle,
            ),
        ];

        print_spaced(
            ctx,
            &mut canvas,
            &blocks,
            Point2D {
                x: (WINDOW_WIDTH * TILE_WIDTH) / 2,
                y: (WINDOW_HEIGHT * TILE_HEIGHT) / 2 - 100,
            },
        );
        canvas.finish(ctx)?;

        Ok(())
    }
}
