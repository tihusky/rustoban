use super::winning::WinningScene;
use super::Scene;
use crate::prelude::*;

pub struct PlayingScene {
    levels: LevelManager,
    current_level: usize,
    sprites: SpriteManager,
    player: Player,
    player_delta: Point2D,
    moves: Vec<PlayerMove>,
    boxes: Vec<MovableBox>,
}

impl PlayingScene {
    pub fn new(ctx: &Context) -> Self {
        let mut s = Self {
            levels: LevelManager::new(),
            current_level: 0,
            sprites: SpriteManager::new(),
            player: Player::new(Point2D { x: 0, y: 0 }),
            player_delta: Point2D { x: 0, y: 0 },
            moves: Vec::new(),
            boxes: Vec::new(),
        };

        s.levels
            .load_from_file(ctx, "/levels.txt")
            .expect("Error loading levels file");

        s.sprites
            .add_sprite(ctx, "wall", "/graphics/wall.png")
            .expect("Error loading wall sprite");
        s.sprites
            .add_sprite(ctx, "floor", "/graphics/floor.png")
            .expect("Error loading floor sprite");
        s.sprites
            .add_sprite(ctx, "target", "/graphics/target.png")
            .expect("Error loading target sprite");
        s.sprites
            .add_sprite(ctx, "player_up", "/graphics/player_up.png")
            .expect("Error loading player_up sprite");
        s.sprites
            .add_sprite(ctx, "player_down", "/graphics/player_down.png")
            .expect("Error loading player_down sprite");
        s.sprites
            .add_sprite(ctx, "player_left", "/graphics/player_left.png")
            .expect("Error loading player_left sprite");
        s.sprites
            .add_sprite(ctx, "player_right", "/graphics/player_right.png")
            .expect("Error loading player_right sprite");
        s.sprites
            .add_sprite(ctx, "box", "/graphics/box01.png")
            .expect("Error loading box sprite");
        s.sprites
            .add_sprite(ctx, "box_on_target", "/graphics/box02.png")
            .expect("Error loading box_on_target sprite");

        s.reset_level();

        s
    }

    fn undo_last_move(&mut self) {
        if let Some(m) = self.moves.pop() {
            *self.player.get_position_mut() -= m.delta;

            if let Some(id) = m.box_id {
                self.boxes
                    .iter_mut()
                    .filter(|b| *b.get_id() == id)
                    .for_each(|b| *b.get_position_mut() -= m.delta);
            }
        }
    }

    fn reset_level(&mut self) {
        let level = self.levels.get_level(self.current_level).unwrap();

        self.player = Player::new(level.player);
        self.player_delta = Point2D { x: 0, y: 0 };
        self.boxes.clear();
        self.moves.clear();

        for pos in &level.boxes {
            self.boxes.push(MovableBox::new(*pos));
        }
    }
}

impl Scene for PlayingScene {
    fn handle_events(&mut self, events: &mut VecDeque<GameEvent>) {
        while let Some(event) = events.pop_front() {
            match event {
                GameEvent::KeyPress(KeyCode::Up, false) => {
                    self.player_delta = Point2D { x: 0, y: -1 };
                }
                GameEvent::KeyPress(KeyCode::Down, false) => {
                    self.player_delta = Point2D { x: 0, y: 1 };
                }
                GameEvent::KeyPress(KeyCode::Left, false) => {
                    self.player_delta = Point2D { x: -1, y: 0 };
                }
                GameEvent::KeyPress(KeyCode::Right, false) => {
                    self.player_delta = Point2D { x: 1, y: 0 };
                }
                GameEvent::KeyPress(KeyCode::Back, false) => {
                    self.undo_last_move();
                }
                GameEvent::KeyPress(KeyCode::R, false) => {
                    self.reset_level();
                }
                _ => (),
            }
        }
    }

    fn update(&mut self, events: &mut VecDeque<GameEvent>) {
        if self.player_delta.x != 0 || self.player_delta.y != 0 {
            if let Ok(m) = self.player.try_move(
                self.player_delta,
                self.levels.get_level(self.current_level).unwrap(),
                &self.boxes,
            ) {
                if let Some(id) = m.box_id {
                    self.boxes
                        .iter_mut()
                        .filter(|b| *b.get_id() == id)
                        .for_each(|b| {
                            *b.get_position_mut() += m.delta;
                        })
                }

                self.moves.push(m);
            }

            self.player_delta = Point2D { x: 0, y: 0 };
        }

        if self
            .levels
            .get_level(self.current_level)
            .unwrap()
            .is_solved(&self.boxes)
        {
            let scene = WinningScene::new(self.moves.len() as i32);
            events.push_back(GameEvent::PushScene(Box::new(scene)));

            self.current_level = (self.current_level + 1) % self.levels.num_levels();
            self.reset_level();
        }
    }

    fn draw(&self, ctx: &mut Context) -> GameResult {
        let mut canvas = Canvas::from_frame(ctx, Color::BLACK);
        let level = self.levels.get_level(self.current_level).unwrap();
        let offset = Point2D {
            x: (WINDOW_WIDTH - level.width) / 2,
            y: (WINDOW_HEIGHT - level.height) / 2,
        };

        level.draw(&self.sprites, &mut canvas, offset);
        self.player.draw(&self.sprites, &mut canvas, offset);

        for b in &self.boxes {
            b.draw(&self.sprites, &mut canvas, offset, &level.targets);
        }

        let hints = vec![
            TextBlock::new(
                TextFragment::new("Press BACKSPACE to undo last move")
                    .color(Color::WHITE)
                    .scale(16.0),
                (16.0, 0.0, 0.0, 0.0),
                TextAlign::Begin,
            ),
            TextBlock::new(
                TextFragment::new("Press R to reset level")
                    .color(Color::WHITE)
                    .scale(16.0),
                (8.0, 0.0, 0.0, 0.0),
                TextAlign::Begin,
            ),
        ];

        let moves = vec![TextBlock::new(
            TextFragment::new(format!("Moves: {}", self.moves.len()))
                .color(Color::WHITE)
                .scale(16.0),
            (16.0, 0.0, 0.0, 0.0),
            TextAlign::End,
        )];

        print_spaced(ctx, &mut canvas, &hints, Point2D { x: 16, y: 0 });
        print_spaced(
            ctx,
            &mut canvas,
            &moves,
            Point2D {
                x: TILE_WIDTH * WINDOW_WIDTH - 16,
                y: 0,
            },
        );

        canvas.finish(ctx)
    }
}
