mod playing;
mod winning;

use crate::prelude::*;
pub use playing::PlayingScene;
pub use winning::WinningScene;

pub trait Scene {
    fn handle_events(&mut self, events: &mut VecDeque<GameEvent>);
    fn update(&mut self, events: &mut VecDeque<GameEvent>);
    fn draw(&self, ctx: &mut Context) -> GameResult;
}
