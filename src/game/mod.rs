pub mod gfx;
pub mod level;
pub mod movable_box;
pub mod player;
pub mod scene;
pub mod state;

use crate::game::scene::Scene;
use crate::prelude::*;

pub enum GameEvent {
    KeyPress(KeyCode, bool),
    ButtonClick(MouseButton, f32, f32),
    PushScene(Box<dyn Scene>),
    PopScene,
}
