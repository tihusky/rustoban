use crate::game::scene::PlayingScene;
use crate::game::scene::Scene;
use crate::prelude::*;

pub struct State {
    scenes: Vec<Box<dyn Scene>>,
    events: VecDeque<GameEvent>,
}

impl State {
    pub fn new(ctx: &Context) -> Self {
        let mut s = Self {
            scenes: Vec::new(),
            events: VecDeque::new(),
        };

        s.scenes.push(Box::new(PlayingScene::new(ctx)));

        s
    }
}

impl EventHandler for State {
    fn key_down_event(
        &mut self,
        _: &mut Context,
        input: input::keyboard::KeyInput,
        repeated: bool,
    ) -> GameResult {
        self.events
            .push_back(GameEvent::KeyPress(input.keycode.unwrap(), repeated));

        Ok(())
    }

    fn mouse_button_up_event(
        &mut self,
        _: &mut Context,
        button: MouseButton,
        x: f32,
        y: f32,
    ) -> GameResult {
        self.events.push_back(GameEvent::ButtonClick(button, x, y));

        Ok(())
    }

    fn update(&mut self, _: &mut Context) -> GameResult {
        if let Some(scene) = self.scenes.last_mut() {
            scene.handle_events(&mut self.events);
            scene.update(&mut self.events);

            // Handle events created by the current scene
            while let Some(event) = self.events.pop_front() {
                match event {
                    GameEvent::PopScene => {
                        self.scenes.pop();
                    }
                    GameEvent::PushScene(s) => {
                        self.scenes.push(s);
                    }
                    _ => (),
                }
            }

            Ok(())
        } else {
            Err(GameError::CustomError("Scene stack is empty!".to_owned()))
        }
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        if let Some(scene) = self.scenes.last() {
            scene.draw(ctx)?;

            Ok(())
        } else {
            Err(GameError::CustomError("Scene stack is empty!".to_owned()))
        }
    }
}
